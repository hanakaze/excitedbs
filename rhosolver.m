function dy= rhosolver(t,y,x,A,B)
    global l;
    a = interp1(x',A,t);
    b = interp1(x',B,t);
    dy = zeros(2,1);
    dy(1) = a*(-1.0/b+(l/2)*(y(2)^2)+1)*y(2) + (a*(y(2)^2+(l/4)*(y(2)^4))*t - 1/t - a/t)*y(1);
    dy(2) = y(1);
end
