function eb_solution

clc;                                               % Clears the screen
clear all;

eb_n = input('excited state number');
global l;
l = 0;% 
a= [];
a = [0.1:0.01:0.15];
a = [a 0.151:0.001:0.399];
a = [a 0.4:0.01:1.00];
% a = [0.54:0.01:1.00]


x = input('starting value of x_end ');
% h=0.01;    % step size0
% N = 3000;
% x = linspace(h,N*h,N);
% A = zeros(1,N);
% B = zeros(1,N);
% sig = zeros(1,N);
% dsig = zeros(1,N);
fn = '';


%if B is too large, rho - > -inf
%if B is too small rho-> +inf

A0= 1.0;                                         % initial condition
dsig0 = 0;  
B_t = 1.0;
a_t = x(end);
options = odeset('RelTol',1e-8,'Events',@events);

for i = 1:length(a)
    sig0 = a(i);
    B_i = 0.0;
    a_t = x(end);
    is_solution = false;
     while(~is_solution)
        
        B0 = (B_i + B_t)/2.0;
        
        initt = [A0 B0 dsig0 sig0];
       
        [T,Y,TE,YE,IE] = ode45(@runge, [0.001,a_t], initt, options);
           
        x= T;
        A = Y(:,1);
        B = Y(:,2);
        dsig = Y(:,3);
        sig = Y(:,4);
  
        %     disp(num2str(B0));
        
        [is_solution,B_i,B_t] = solution_judger(eb_n,TE,IE,B0,B_i,B_t,sig);
     
        %if B is too large, rho - > -inf
        %if B is too small rho-> +inf
    end
    disp('solution found');
  
%     plot(x,sig);
    fn = sprintf('eb=%d l=%0.1f_sig0=%0.3f_B=%0.6f_solution',eb_n,l,sig0,B0);
    fn = [fn '.mat'];
    save(fn,'l','x','A','B','sig','dsig');
end
end

%-------------------
% function [node_dsig,node_sig] = nodes_info(TE,IE)
%     node_dsig = [];
%     node_sig = [];
%     for i= 1:length(IE)
%         if IE(i) == 1
%             node_dsig(end+1) = TE(i);
%         else node_sig(end+1) = TE(i);
%         end
%     end
% end

function [value,isterminal,direction] = events(t,y)
    value = [y(3) y(4)];     % Detect height = 0
    isterminal = [0 0];   % Stop the integration
    direction = [0 0];
end

 function [is_solution,B_i,B_t] = solution_judger(eb_n,TE,IE,B0,B_i,B_t,sig)
        [node_dsig,node_sig] = nodes_info(TE,IE);
        n1 = length(node_sig);
        n2 = length(node_dsig);
        if (n1 == eb_n)
            if (abs(sig(end))< 1e-4) & (n2 == (eb_n+1))
                is_solution =true;
                
            elseif sig(end) < 0
%                 disp(strcat('B0 = ', num2str(B0),'too large'));
                if mod(eb_n,2) ==0
                    B_i = B0;
                else
                    B_t = B0;
                end
                is_solution = false;
                
            elseif sig(end) > 0
%                 disp(strcat('B0 = ', num2str(B0),'too small'));
                if mod(eb_n,2) ==0
                    B_t = B0;
                else
                    B_t = B0;
                end
                is_solution = false;
            end
        elseif (n1 > eb_n)
%             disp(strcat('B0 = ', num2str(B0),'too small'));
            B_i = B0;
            is_solution = false;
        else
%             disp(strcat('B0 = ', num2str(B0),'too large'));
            B_t = B0;
            is_solution = false;
        end

    function [node_dsig,node_sig] = nodes_info(TE,IE)
        node_dsig = [];
        node_sig = [];
        for i= 1:length(IE)
            if IE(i) == 1
                node_dsig(end+1) = TE(i);
            else node_sig(end+1) = TE(i);
            end
        end
    end
 end

