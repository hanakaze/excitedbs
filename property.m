X=[x',Y];
r = x;
hr = h ;
rho=(1.0 + 1./ X(:,3)).*X(:,5).*X(:,5) + (l/4).*X(:,5).*X(:,5).*X(:,5).*X(:,5) + X(:,4).*X(:,4)./X(:,2); %energy density
rho0= rho(1); %central density
 %central particle number
nd=2.*X(:,5).*X(:,5)./sqrt(X(:,3)); %particle number density
mass=dot(rho,r.*r)*hr/2;
pn=dot(nd.*sqrt(X(:,2)),r.*r)*hr/2;
