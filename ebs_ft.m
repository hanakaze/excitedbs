% Before Run, besure to check:
% lambda value 
fn=importdata('l0.0_sig_B.csv'); %file names_B.csv');%file names
Lf = length(fn);

N = 5000;
r = zeros(1,N);
efr=zeros(Lf, 1);
mass = zeros(Lf, 1);
pn = zeros(Lf, 1);
rho0 = zeros(Lf,1);
pn0 =zeros(Lf,1);
sig0 = zeros(Lf,1);

handler = figure;
cutoff=[1.0];
Ncut = length(cutoff);
S = zeros(Lf, Ncut);
legends = cell(Ncut,1);

for i=1:Lf%number of files

            load(char(fn(i)));
            
			r = linspace(x(1),x(end),N);
            r = r';
            hr = r(2)-r(1);
            A = interp1(x,A,r);
            B = interp1(x,B,r);
            sig = interp1(x,sig,r);
            dsig = interp1(x,dsig,r);
            
        
			% pr=(dsig.^2)+(sig.^2).*A./B-((sig.^2)+(sig.^4)*(l/4.0)).*A;
			% [prv,pri]=sort(abs(pr));
			% X=X(1:pri(1),:);
			% r=r(1:pri(1),:);
			sig0(i) = sig(1);

			rho=(1.0 + 1./ B).*sig.*sig + (l/4).*sig.*sig.*sig.*sig + dsig.*dsig./A; %energy density
			rho0(i) = rho(1); %central density
			
			nd=2.*sig.*sig./sqrt(B); %particle number density
    		pn(i)=dot(nd.*sqrt(A),r.*r)*hr/2;
    		pn0(i) = pn(1); %central particle number

    		mass(i)=dot(rho,r.*r)*hr/2;

			efr(i)=dot(rho,r.*r.*r)/dot(rho,r.*r);%radius r^3

			% pr=(dsig.^2)+(sig.^2).*A./B-((sig.^2)+(sig.^4)*(l/4.0)).*A; %pressure
			% efr(i)=dot(rho,r.*r.*r.*r)/dot(rho,r.*r);efr(i)=sqrt(efr(i));
			% efr(i)=X(find(rho<=rho(1)*exp(-1),1),1);
			% efr(i)=X(find(dsig==min(dsig),1),1);
			% efrin=find(dsig==min(dsig));
			% rg11=sqrt(X(:,1));
			% efr(i)=sum(rg11(1:efrin))*hr;

			R = rho; %energy density
			% R=((1.0 + 1./ B).*sig.*sig + (l/4).*sig.*sig.*sig.*sig + dsig.*dsig./A).*sqrt(A.*B); %sqrt(g)energy density
			% R=(((1.0 + 1./ B).*sig.*sig + (l/4).*sig.*sig.*sig.*sig + dsig.*dsig./A)-2.*sqrt(A).*sig.*sig./sqrt(B));%energy density-sqrt(A)n
			% R=(((1.0 + 1./ B).*sig.*sig + (l/4).*sig.*sig.*sig.*sig + dsig.*dsig./A)-2.*sig.*sig./sqrt(B));%energy density-n
			% R=(((1.0 + 1./ B).*sig.*sig + (l/4).*sig.*sig.*sig.*sig + dsig.*dsig./A)).*(sqrt(A.*B)-1)%material energy density
			


for j=1:1:Ncut
            hk=0.001;
			k=linspace(hk,10*pi/(efr(i)),2001);
			% k=linspace(0.001/efr(i),10*pi/efr(i),2001);
			k=k';
			hk=k(2)-k(1);
			
 			K = (k.^(-1)*r').*sin(k*r')*hr;
			fft= K*R;%fourier mode
            
            % fk=abs(fft);
			fk=fft.*fft;    %modal fraction
            
% 			ftot=dot(fk,k.*k)
            
            % fkmax=(hr*dot(r.*r,R))^2;%the most weight at k=0
			fkmax=max(fk);
% 			fkmax=ftot; %normalize to 1
			
			fk=fk/fkmax;%first most weight
%         
						
			S(i,j)=dot((fk.*log(fk)),(k.*k));
			S(i,j)=-4*pi*hk*S(i,j);
			% legends{j,1} = strcat('t0=',num2str(t0(i)),'Cutoff=', num2str(cutoff(j)));
			% save(strcat('phi0=',num2str(phi(i)),'.mat'),'k','fft','fk','r','R');
    end
    
end
save('EBS_l0_10efr_1cutoff_5k.mat','sig0','cutoff','S','efr','rho0','pn0','pn','mass');
% plot(phi,S),xlabel('phi(0)'),ylabel('Entropy'), legend(legends);
% saveas(handler,'sqrtphiS.fig');
% quit;