fn = importdata('filenames.csv')
Nfn = length(fn);
figure;
hold on;
legends = cell(Nfn,1);
handlers = zeros(Nfn,1);
for i=1:Nfn
    load(fn{i,1});
    x= sig0;
    y = S./rho0;
    handlers(i) = plot(x,y);
    legends{i,1} = strcat(num2str(i-1),' excited state');
    [PLS,LOCS] = findpeaks(y);
    plot(x(LOCS),y(LOCS),'d','MarkerSize',10);
    [PLS,LOCS] = findpeaks(-y);
    plot(x(LOCS),y(LOCS),'d','MarkerSize',10);
end
legend(handlers,legends);
    